##PROJECT D: Riner J. Bonilla, 21641297
##SISTEMAS OPERATIVOS

#Funciones:
- Esta WriteSector en Kernerl con su syscall Funcionando
- Esta DeleteFile en Kernel Funcionando
- Esta WriteFile en kernel Funcionando
- Esta copyFile en shell Funcionando
- Esta el comando "dir" de listar archivos Funcionando
- Esta CreateFile en kernel Funcionando

##NOTE: el SO tiene un comando extra "help" que imprime todos los comandos disponibles y como usarlos en dicho SO.

#Pasos para Compilar SO:

1. Correr script kerni.sh "./kerni.sh" desde "~/projectd", este script compila kernel y lo escribe en floppya.img, compila shell y compila tstprg.

2. Abrir el driver de FUSE y reemplazar el ejecutable "shell" existente por el "shell" compilado desde "~/projectd" y cerrar FUSE.

3. Una vez con el shell en el floppya.img, correr el SO con Bochs y Listo.
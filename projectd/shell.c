int cmpstr(char* file, char* dir, int size);
void shell(char* file);
char *
itoa (int value, char *result, int base);

int main(){
    int g = 0;
    char str[80];
    syscall_printString("SHELL>");
    syscall_readString(str);
    syscall_printString("\r\n");
    syscall_printString(str);
    syscall_printString("\r\n");
    shell(str);
    for(;g<80;g++){
        str[g] = '\0';
    }
    syscall_terminate();
    return 0;
}

int cmpstr(char* file, char* dir, int size){
    int j;
    int ef =1;
    for(j = 0; j< size ;j++){
        if(file[j]!=dir[j]){
            ef = 0;
            break;
        }
    }
    if(ef) 
        return 0;
    return -1;
}

void shell(char* file){
    char buff[13312];
    if(cmpstr(file,"clear",5)==0){
        syscall_clearScreen();
    }
    else if(cmpstr(file,"type",4)==0){
        syscall_printString("\r\n");
        syscall_readFile(&file[5],buff);
        syscall_printString(buff);
    }
    else if(cmpstr(file,"execute",7)==0){
        syscall_executeProgram(&file[8],0x2000);
    }
    else if(cmpstr(file,"delete",6)==0){
        syscall_deleteFile(&file[7]);
    }
    else if(cmpstr(file,"help",4)==0){
        syscall_printString("- clear");
        syscall_printString("\r\n");
        syscall_printString("- type <file>");
        syscall_printString("\r\n");
        syscall_printString("- delete <file>");
        syscall_printString("\r\n");
        syscall_printString("- execute <program>");
        syscall_printString("\r\n");
        syscall_printString("- copy <file> <file_new>");
        syscall_printString("\r\n");
         syscall_printString("- create <filename>");
        syscall_printString("\r\n");
    }
    else if(cmpstr(file,"copy",4)==0){
        int sect = 0;
        int newna = 5;
        char snum[5];
        char origi[7];
        int g=0;
        int fef = 0;

        while(file[newna]!=' '){
            newna++;
        }
        newna++;
        for(g=5;g<newna;g++,fef++){
            origi[fef] = file[g];
        }
        origi[fef]='\0';
        syscall_printString("origi: ");
        syscall_printString(origi);
        syscall_printString("+\r\n");

        syscall_readFile(&file[5],&buff[0]);
        syscall_printString(buff);
        syscall_printString("\r\n");

        sect = getNumSect(&file[5]);
        itoa(sect,snum,10);
        syscall_printString("num of sect:");
        syscall_printString(snum);
        syscall_printString("\r\n");

        if(sect!=0){
            syscall_writeFile(&file[newna],buff,sect);
            syscall_printString("file copied");
            syscall_printString("\r\n");
            snum[0] ='\0';
        }
        else{
            syscall_printString("no sectors found");
            syscall_printString("\r\n");
            snum[0] ='\0';
        }
        
    }
    else if(cmpstr(file,"dir",3)==0){
        int c;
        int j;
        char dir[512];
        char filer[7];
        syscall_readSector(dir,2);

        for(c = 0; c<512;c=c+32){
            if(dir[c]==0x00){
                continue;
            }
            for(j = 0; j< 6;j++){
                if(dir[c+j]==0){
                    filer[j]='\0';
                    break;
                }
                filer[j] = dir[c+j];
            }
            filer[6]='\0';
            syscall_printString("/");
            syscall_printString(filer);
            syscall_printString("\r\n");
            filer[0] = '\0';
        }


    }
    else if(cmpstr(file,"create",6)==0){
        syscall_createFile(&file[7]);
    }
    buff[0]='\0';
}
int foundit(char* file, char* dir){
    int c;
    int j;
    int ef;
    for(c = 0; c<512;c=c+32){ /*16 entries MAX(files)*/
        ef = 1;
        for(j = c; j< c+6;j++){ /*load file name*/
            if(file[j%32]!=dir[j]){ /*if not matched, next*/
                ef = 0;
                break;
            }
        }
        if(ef) 
            break; /*if found, leave*/
    }
    if(ef) 
        return c; /*return pos of file in directory*/
    return -1;
}

int getNumSect(char* dir){
    char snum[5];
    char buff[512]; /*directory sector*/
    int sekk=0; /*n-sectors*/
    int c =0; /*pos of file in directory*/
    int ef=1;
    int count=0;
    int regi;
    char dir2[6];

    syscall_printString("name:");
    syscall_printString(dir);
    syscall_printString("\r\n");

    for(;count<6;count++){
        dir2[count] = dir[count];
    }

    count =0;

    for(;count<6;count++){
        if(dir2[count]==' '){
            dir2[count]= 0x00;
            regi = count+1;
        }
        if(regi == count && regi!=0){
            dir2[count]=0x00;
            regi++;
        }
    }

    syscall_printString("new_name:");
    syscall_printString(dir2);
    syscall_printString("\r\n");

    syscall_readSector(buff,2); /*load directory sector*/

    c = foundit(dir2,buff);
    if(c==-1) ef = 0;

    itoa(ef,snum,10);
    syscall_printString("found:");
    syscall_printString(snum);
    syscall_printString("\r\n");
    if(ef){

        for(c=c+6; c%32!=0 && buff[c]!=0 ;c++){ /*c(pos) until end of file size(32) and sector of file not empry(00)*/
            sekk++;
        }
        return sekk;
    }
    else{
        return 0;
    }
}

char *
itoa (int value, char *result, int base)
{
    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return result; }

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while (ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}
int main(){
    char buffer[13312];
    syscall_printString("this is test.c");
    syscall_printString("\r\n");
    /*this is the maximum size of a file*/
    syscall_readFile("messag", buffer);
    /*read the file into buffer*/
    syscall_printString(buffer);
    /*print out the file*/
    syscall_terminate();
    
    return 0;
}
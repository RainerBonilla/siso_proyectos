;printchar.asm
;Rainer Bonilla, 20XX

	.global _printchar

;void printchar(char ch) 
_printchar:
    push bp
    mov bp, sp
    push ds

    mov al,[bp+4] 
    mov ah, 0x0e 
    int 0x10

    pop ds
    mov sp, bp
    pop bp
    ret
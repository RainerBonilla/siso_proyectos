void putInMemory (int segment, int address, char character);
void clearScreen();
void printString(char* c);
void readString(char *c);

int main()
{
    /*char line[80];
    clearScreen();
    printString("Enter a line: \0");
    readString(line);
    printString(line);*/

    /*char buffer[512];

    clearScreen();
    printString("print before buffer...");
    readSector(buffer, 30);
    printString(buffer);
    printString("print after buffer...");*/
    makeInterrupt21();
    loadProgram();
    /*while(1){}*/
    return 0;
}

void clearScreen(){
    int k =0;
    int clear = 0x8000;

    for(k=0;k<2000;k++){
        putInMemory(0xB000, clear, ' ');
        clear = clear + 0x1;
        putInMemory(0xB000, clear, 0x7);
        clear = clear + 0x1;
    }
}

void printString(char* c){
    int n = 0;

    while (c[n] != '\0') {
      printchar(c[n]);
      n++;
   }
}

void readString(char* c){
    char n;
    int a=0;

    while (n != 0xd) {
      n = readchar();
      if(n==0x8){
        printchar(n);
        a--;
      }
      else{
        printchar(n);
        c[a] = n;
      }
      a++;
   }

   c[a] = '\0';
}

void handleInterrupt21 (int AX, int BX, int CX, int DX){
    switch(AX){
        case 0:
        printString((char*)BX);
        break;
        case 1:
        readString((char*)BX);
        break;
        case 2:
        readSector((char*)BX,CX);
        break;
        default:
        printString("ERROR");
        break;
    }
}
    .global _syscall_printString
	.global _syscall_readString
	.global _syscall_readSector


;syscall_printString(char *str)
_syscall_printString:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x0
    mov bx, [bp+4]
    int #0x21

    pop ds
	pop bp
	ret

;syscall_readString(char *str)
_syscall_readString:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x1
    mov bx, [bp+4]
    int #0x21

    pop ds
	pop bp
	ret

;syscall_readSector(char *buffer, int sector)
_syscall_readSector:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x2
    mov bx, [bp+4]
    mov cx, [bp+6]
    int #0x21

    pop ds
	pop bp
	ret

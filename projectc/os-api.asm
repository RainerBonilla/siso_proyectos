    .global _syscall_printString
	.global _syscall_readString
	.global _syscall_readSector
    .global _syscall_readFile
    .global _syscall_executeProgram
    .global _syscall_terminate
    .global _syscall_clearScreen

;syscall_printString(char *str)
_syscall_printString:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x0
    mov bx, [bp+4]
    int #0x21

    pop ds
	pop bp
	ret

;syscall_clearScreen()
_syscall_clearScreen:
    push bp
	mov bp,sp
	push ds

    mov ax, #0xa
    int #0x21

    pop ds
	pop bp
	ret

;syscall_terminate()
_syscall_terminate:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x5
    int #0x21

    pop ds
	pop bp
	ret

;syscall_readString(char *str)
_syscall_readString:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x1
    mov bx, [bp+4]
    int #0x21

    pop ds
	pop bp
	ret

;syscall_readFile(char *dir, char* buffer)
_syscall_readFile:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x3
    mov bx, [bp+4]
    mov cx, [bp+6]
    int #0x21

    pop ds
	pop bp
	ret

;syscall_executeProgram(char *name, int segment)
_syscall_executeProgram:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x4
    mov bx, [bp+4]
    mov cx, [bp+6]
    int #0x21

    pop ds
	pop bp
	ret

;syscall_readSector(char *buffer, int sector)
_syscall_readSector:
    push bp
	mov bp,sp
	push ds

    mov ax, #0x2
    mov bx, [bp+4]
    mov cx, [bp+6]
    int #0x21

    pop ds
	pop bp
	ret

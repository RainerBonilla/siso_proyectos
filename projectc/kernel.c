void putInMemory (int segment, int address, char character);
void clearScreen();
void printString(char* c);
void readString(char *c);
void executeProgram(char* filename, int seg);

int main()
{
    /*char line[80];
    clearScreen();
    printString("Enter a line: \0");
    readString(line);
    printString(line);*/

    /*char buffer[512];

    clearScreen();
    printString("print before buffer...");
    readSector(buffer, 30);
    printString(buffer);
    printString("print after buffer...");*/
    /*makeInterrupt21();
    loadProgram();
    while(1);*/
    makeInterrupt21();
    executeProgram("shell", 0x2000);
    while(1);
    return 0;
}

void clearScreen(){
    int k =0;
    int clear = 0x8000;

    for(k=0;k<2000;k++){
        putInMemory(0xB000, clear, ' ');
        clear = clear + 0x1;
        putInMemory(0xB000, clear, 0x7);
        clear = clear + 0x1;
    }
    setInit();
}

void printString(char* c){
    int n = 0;

    while (c[n] != '\0') {
      printchar(c[n]);
      n++;
   }
}

void readString(char* c){
    char n;
    int a=0;

    while (n != 0xd) {
      n = readchar();
      if(n==0x8 && a>0){
        printchar(n);
        printchar(' ');
        printchar(n);
        a--;
      }
      if(n!=0x8){
        printchar(n);
        c[a] = n;
        a++;
      }
   }

   c[a] = '\0';
}

int compareStr(char* a, char* b){
    int x = 5;
    int count = 5;

    while(count!=0){
        if(a[x]==b[x]){
            x--;
            count--;
        }
        else{
            count=0;
        }
    }
    if(x==0){
        return 1;
    }
    else{
        return 0;
    }
}
int foundit(char* file, char* dir){
    int c;
    int j;
    int ef;
    for(c = 0; c<512;c=c+32){ /*16 entries MAX(files)*/
        ef = 1;
        for(j = c; j< c+6;j++){ /*load file name*/
            if(file[j%32]!=dir[j]){ /*if not matched, next*/
                ef = 0;
                break;
            }
        }
        if(ef) 
            break; /*if found, leave*/
    }
    if(ef) 
        return c; /*return pos of file in directory*/
    return -1;
}
void readFile(char* dir, char* buffer){
    char buff[512]; /*directory sector*/
    int sekk=0; /*n-sectors*/
    int c =0; /*pos of file in directory*/
    int ef=1;

    readSector(buff,2); /*load directory sector*/

    c = foundit(dir,buff);
    if(c==-1) ef = 0;

    if(ef){
        for(c=c+6; c%32!=0 && buff[c]!=0 ;c++){ /*c(pos) until end of file size(32) and sector of file not empry(00)*/
            readSector(&buffer[sekk],buff[c]);
            sekk = sekk+512;
        }
    }
    else{
        printString("file not found");
    }
}
void executeProgram(char* filename, int seg){
    char buffer[13312];
    int buffpos =0;
    if(seg>0x1000 && seg<0xA000){
        readFile(filename,buffer);
        if(buffer[0]!=0){
            for(;buffpos<13312;buffpos++){
                putInMemory(seg,buffpos,buffer[buffpos]);
            }
            launchProgram(seg);
        }
    }
    else{
        printString("not aviable");
    }
}

void terminate(){
    char shell[6];
    shell[0] = 's';
    shell[1] = 'h';
    shell[2] = 'e';
    shell[3] = 'l';
    shell[4] = 'l';
    shell[5] = '\0';
    executeProgram(shell,0x2000);
}
void handleInterrupt21 (int AX, int BX, int CX, int DX){
    switch(AX){
        case 0:
        printString((char*)BX);
        break;
        case 1:
        readString((char*)BX);
        break;
        case 2:
        readSector((char*)BX,CX);
        break;
        case 3:
        readFile((char*)BX,(char*)CX);
        break;
        case 4:
        executeProgram((char*)BX,CX);
        break;
        case 5:
        terminate();
        break;
        case 0xa:
        clearScreen();
        break;
        default:
        printString("ERROR");
        break;
    }
}
int cmpstr(char* file, char* dir, int size);
void shell(char* file, char* buff);

int main(){
    char str[80], buffer[13312];
    syscall_printString("SHELL>");
    syscall_readString(str);
    syscall_printString("\r\n");
    syscall_printString(str);
    syscall_printString("\r\n");
    shell(str,buffer);
    buffer[0] = '\0';
    syscall_terminate();
    return 0;
}

int cmpstr(char* file, char* dir, int size){
    int j;
    int ef =1;
    for(j = 0; j< size ;j++){
        if(file[j]!=dir[j]){
            ef = 0;
            break;
        }
    }
    if(ef) 
        return 0;
    return -1;
}

void shell(char* file, char* buff){
    if(cmpstr(file,"clear",5)==0){
        syscall_clearScreen();
    }
    else if(cmpstr(file,"type",4)==0){
        syscall_readFile(&file[5],buff);
        syscall_printString(buff);
    }
    else if(cmpstr(file,"execute",7)==0){
        syscall_executeProgram(&file[8],0x2000);
    }
    else{
        syscall_printString("Bad Command!");
        syscall_printString("\r\n");
    }
}
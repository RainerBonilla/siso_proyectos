class Phil implements Runnable {
    private Object Lechop;
    private Object Rechop;

    public Phil(Object Lechop,Object Rechop) {
        this.Lechop = Lechop;
        this.Rechop = Rechop;
    }

    public void run(){
        try {
            while(true){
                Act("Is Thinking bruh");
                //monitor
                synchronized(Lechop){
                    Act("Takes left chop");
                    synchronized(Rechop){
                        Act("Takes Right chop = EAT TIME BAYBEE");
                        Act("*Drops Right chop*");
                    }
                    Act("*Drops Left Chop*");
                    Act("JEE WHEEZ..., back to thinkin' bruh");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return;
        }
    }
    
    private void Act(String act) throws InterruptedException{
        System.out.println(Thread.currentThread().getName()+" "+act);
        Thread.sleep(500);
    }
}

public class DininPhils{
    public static void main(String[] args) throws Exception {
        Phil[] bruhs = new Phil[5];
        Object[] chopz = new Object[bruhs.length];

        for(int i = 0; i < chopz.length; i++){
            chopz[i] = new Object();
        }
        for(int i = 0; i < bruhs.length; i++){
            Object lefty = chopz[i];
            Object righty = chopz[(i+1)%chopz.length];

            if(i==bruhs.length-1){
                bruhs[i] = new Phil(righty,lefty);
            }
            else{
                bruhs[i] = new Phil(lefty,righty);
            }

            Thread riceball = new Thread(bruhs[i], "Philo' "+(i+1));
            riceball.start();
        }
    }
}

void putInMemory (int segment, int address, char character);
void clearScreen();
void printString(char* c);
void readString(char *c);
void executeProgram(char* filename, int fg);
void deleteFile(char* file);
void handleTimerInterrupt(int segment, int sp);

struct Entry{
    int active;
    int stackptr;
    int waiting;
};

struct Entry entries[8];
int currentProcess;
int frst;
int zeit;

int main()
{
    int i=0;
    /*char line[80];
    clearScreen();
    printString("Enter a line: \0");
    readString(line);
    printString(line);*/

    /*char buffer[512];

    clearScreen();
    printString("print before buffer...");
    readSector(buffer, 30);
    printString(buffer);
    printString("print after buffer...");*/
    /*makeInterrupt21();
    loadProgram();
    while(1);*/
    makeInterrupt21();
    for(;i<8;i++){
        entries[i].active = 0;
        entries[i].stackptr = 0xff00;
        entries[i].waiting = -1;
    }
    frst = 1;
    zeit = 100;
    currentProcess = 0;
    executeProgram("shell",0);
    makeTimerInterrupt();
    while(1);
    return 0;
}

void clearScreen(){
    int k =0;
    int clear = 0x8000;

    for(k=0;k<2000;k++){
        putInMemory(0xB000, clear, ' ');
        clear = clear + 0x1;
        putInMemory(0xB000, clear, 0x7);
        clear = clear + 0x1;
    }
    setInit();
}

void printString(char* c){
    int n = 0;

    while (c[n] != '\0') {
      printchar(c[n]);
      n++;
   }
}
void killProcess(int pro){
    int g;
    if(pro>=0 && pro<8){
        setKernelDataSegment();
        for(g=0;g<8;g++){
            if(entries[g].waiting == pro){
                entries[g].active = 1;
                entries[g].waiting = -1;
            }
        }
        entries[pro].active = 0;
        entries[pro].waiting = -1;
        restoreDataSegment();
    }
}
void readString(char* c){
    char n;
    int a=0;

    while (n != 0xd) {
      n = readchar();
      if(n==0x8 && a>0){
        printchar(n);
        printchar(' ');
        printchar(n);
        a--;
      }
      if(n!=0x8 && n!=0xd){
        printchar(n);
        c[a] = n;
        a++;
      }
   }

   c[a] = '\0';
}

int compareStr(char* a, char* b){
    int x = 5;
    int count = 5;

    while(count!=0){
        if(a[x]==b[x]){
            x--;
            count--;
        }
        else{
            count=0;
        }
    }
    if(x==0){
        return 1;
    }
    else{
        return 0;
    }
}
int foundit(char* file, char* dir){
    int c;
    int j;
    int ef;
    for(c = 0; c<512;c=c+32){ /*16 entries MAX(files)*/
        ef = 1;
        for(j = c; j< c+6;j++){ /*load file name*/
            if(file[j%32]==' '){
                break;
            }
            if(file[j%32]!=dir[j]){ /*if not matched, next*/
                ef = 0;
                break;
            }
        }
        if(ef) 
            break; /*if found, leave*/
    }
    if(ef) 
        return c; /*return pos of file in directory*/
    return -1;
}
void readFile(char* dir, char* buffer){
    char buff[512]; /*directory sector*/
    int sekk=0; /*n-sectors*/
    int c =0; /*pos of file in directory*/
    int ef=1;

    readSector(buff,2); /*load directory sector*/

    c = foundit(dir,buff);
    if(c==-1) ef = 0;

    if(ef){
        for(c=c+6; c%32!=0 && buff[c]!=0 ;c++){ /*c(pos) until end of file size(32) and sector of file not empry(00)*/
            readSector(&buffer[sekk],buff[c]);
            sekk = sekk+512;
        }
    }
    else{
        buffer[0] = 0;
        printString("file not found");
    }
}

void executeProgram(char* filename, int fg){
    int i =0;
    int segi=0;
    char buffer[13312];
    int buffpos =0;
    setKernelDataSegment();
    for(;i<8;i++){
        if(entries[i].active == 0){

            if(fg){
                entries[currentProcess].active = 2;
                entries[currentProcess].waiting = i;
            }
            
            entries[i].active = 1;
            entries[i].stackptr = 0xff00;
            restoreDataSegment();
            segi = i+2;
            segi = segi * 0x1000;
            break;
        }
    }
    if(i==8){
        restoreDataSegment();
        return;
    }
    if(segi>0x1000 && segi<0xA000){
        readFile(filename,buffer);
        if(buffer[0]!=0){
            printchar('y');
            printchar('\r');
            printchar('\n');
            for(;buffpos<13312;buffpos++){
                putInMemory(segi,buffpos,buffer[buffpos]);
            }
            initializeProgram(segi);
            setKernelDataSegment();
            zeit = 100;
            restoreDataSegment();
        }
        else{
            printchar('x');
            printchar('\r');
            printchar('\n');
            setKernelDataSegment();
            entries[i].active = 0;
            restoreDataSegment();
        }
    }
    else{
        printchar('x');
        printchar('x');
        printchar('\r');
        printchar('\n');
        setKernelDataSegment();
        entries[i].active = 0;
        restoreDataSegment();
        printString("not aviable");
    }
}

void terminate(){
    int tempo;
    int i;
    /*char shell[6];
    shell[0] = 's';
    shell[1] = 'h';
    shell[2] = 'e';
    shell[3] = 'l';
    shell[4] = 'l';
    shell[5] = '\0';
    executeProgram(shell,0x2000);*/
    setKernelDataSegment();
    tempo = currentProcess;
    entries[tempo].active = 0;
    entries[tempo].stackptr = 0xff00;
    for(i=0;i<8;i++){
        if(entries[i].active == 2 && entries[i].waiting == currentProcess){
            entries[i].active = 1;
            entries[i].waiting = -1;
        }
    }

    while(1);
}

void deleteFile(char* file){
    char dir[512];
    char map[512];
    int c;
    int ef = 1;

    readSector(map,1);
    readSector(dir,2);

    c = foundit(file,dir);
    if(c==-1) ef = 0;

    if(ef){
        dir[c] = 0x00;
        for(c=c+6; c%32!=0 && dir[c]!=0 ;c++){
            map[(dir[c])] = 0x00;
        }

        writeSector(map,1);
        writeSector(dir,2);
        printString("file deleted");
    }
    else{
        printString("file not found");
    }
}

int foundfree(char* dir){
    int c;
    int j;
    int ef = 0;
    for(c = 0; c<512;c=c+32){
        if(dir[c]==0x00){
            ef = 1;
            break;
        }
    }
    if(ef) 
        return c; /*return pos of file in directory*/
    return -1;
}

int foundfreemap(char* dir){
    int c;
    int j;
    int ef = 0;
    for(c = 0; c<512;c++){
        if(dir[c]==0x00){
            ef = 1;
            break;
        }
    }
    if(ef) 
        return c; /*return pos of file in directory*/
    return -1;
}

void writeFile(char* file, char* content, int sectors){
    char dir[512];
    char map[512];
    int c;
    int m;
    int cu;
    int cont =0;
    int name=0;
    int ef = 1;
    int af = 1;

    readSector(map,1);/*Load the Map and Directory sectors into buffers*/
    readSector(dir,2);

    c = foundfree(dir); /*Find a free directory entry (one that begins with 0x00)*/
    if(c==-1) ef = 0;   

    if(ef && sectors<=26){
        cu = c+6;
        for(;c<cu;c++){ /*Copy the name to that directory entry. If the name is less than 6 bytes, fill in the remaining bytes with 0x00*/
            if(file[name]==0){
                dir[c]=0x00;
                break;
            }
            else{
                dir[c] = file[name];
            }
            name++;
        }
        c = cu;
 
        while(sectors!=0){
            m = foundfreemap(map); /*Find a free sector by searching through the Map for a 0x00*/
            if(m==-1) af = 0;

            if(af){
                map[m] = 0xFF; /*Set that sector to 0xFF in the Map*/
                dir[c] = m; /*Add that sector number to the file's directory entry*/

                writeSector(&content[cont],m); /*Write 512 bytes from the buffer holding the file to that sector*/

                c++;
                cont=cont+512;
            }
            else{
                return;
            }
            sectors--;
        }

        for(; c%32!=0;c++){ /*Fill in the remaining bytes in the directory entry to 0x00*/
            dir[c] = 0x00;
        }

        writeSector(map,1); /*Write the Map and Directory sectors back to the disk*/
        writeSector(dir,2);
    }
}

void createFile(char * file){
    char ster[80], buffy[13312];
    int intbuff =0;
    int buffbuff=0;
    ster[0] = 'a';

    while(ster[0]!=0 && buffbuff < 13312){
        printchar(ster[0]+48);
        readString(ster);
        printchar('\r');
        printchar('\n');
        for(intbuff=0;ster[intbuff]!=0;intbuff++){
            buffy[buffbuff] = ster[intbuff];
            buffbuff++;
        }
        buffy[buffbuff] = '\r';
        buffbuff++;
        buffy[buffbuff] = '\n';
        buffbuff++;
    }

    buffy[buffbuff] = '\0';

    
    if(file[0]!=0 && buffy[0]!=0){
        buffbuff = (buffbuff/512)+1;
        writeFile(file,buffy,buffbuff);
        printchar('V');
        printchar('\r');
        printchar('\n');
    }
    else{
        printchar('X');
        printchar('\r');
        printchar('\n');
    }
}
void handleTimerInterrupt(int segment, int sp){
    int j,k;
    if(zeit == 100){
        zeit = 0;
        entries[currentProcess].stackptr = sp;
        j = currentProcess + 1;

        if(frst){
            zeit++;
            frst = 0;
            returnFromTimer(0x2000,0xff00);
            return;
        }

        for(k=0;k<8;k++,j++){
            if(j%8==currentProcess){
                returnFromTimer(segment,sp);
                return;
            }

            if(entries[j%8].active == 1){
                segment = ((j%8)+2)*0x1000;
                sp = entries[j%8].stackptr;
                currentProcess = j%8;
                returnFromTimer(segment,sp);
                return;
            }
        }
    }
    else{
        zeit++;
        returnFromTimer(segment,sp);
    }
}

void handleInterrupt21 (int AX, int BX, int CX, int DX){
    switch(AX){
        case 0:
        printString((char*)BX);
        break;
        case 1:
        readString((char*)BX);
        break;
        case 2:
        readSector((char*)BX,CX);
        break;
        case 3:
        readFile((char*)BX,(char*)CX);
        break;
        case 4:
        executeProgram((char*)BX,CX);
        break;
        case 5:
        terminate();
        break;
        case 6:
        writeSector((char*)BX,CX);
        break;
        case 7:
        deleteFile((char*)BX);
        break;
        case 8:
        writeFile((char*)BX,(char*)CX,DX);
        break;
        case 9:
        createFile((char*)BX);
        break;
        case 0xa:
        clearScreen();
        break;
        case 11:
        killProcess(BX);
        break;
        default:
        printString("ERROR");
        break;
    }
}
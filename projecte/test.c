int main(){
    char buffer[13312];
    enableInterrupts();
    syscall_printString("this is new test.c");
    syscall_printString("\r\n");
    /*this is the maximum size of a file*/
    syscall_readFile("messag", buffer);
    /*read the file into buffer*/
    syscall_printString(buffer);
    syscall_printString("ingrese algo: ");
    syscall_readString(buffer);
    syscall_printString(buffer);
    syscall_printString("\r\n");
    /*print out the file*/
    syscall_terminate();
    
    return 0;
}
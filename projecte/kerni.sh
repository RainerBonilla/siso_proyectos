bcc -ansi -c -o kernel.o kernel.c
as86 kernel.asm -o kernel_asm.o
ld86 -o kernel -d kernel.o kernel_asm.o
dd if=kernel of=floppya.img bs=512 conv=notrunc seek=3
bcc -ansi -c -o test.o test.c
as86 -o os-api.o os-api.asm
as86 -o lib.o lib.asm
ld86 -o tstprg -d test.o os-api.o lib.o
bcc -ansi -c -o shell.o shell.c
ld86 -o shell -d shell.o os-api.o lib.o
! 1 
! 1 # 1 "foo.c"
! 1     int main ()
! 2 # 1 "foo.c"
! 1 {
export	_main
_main:
! 2     int in  [50];
!BCC_EOS
! 3     int res [50];
!BCC_EOS
! 4 
! 5     int avg=0;
push	bp
mov	bp,sp
push	di
push	si
add	sp,#-$CA
! Debug: eq int = const 0 to int avg = [S+$D0-$D0] (used reg = )
xor	ax,ax
mov	-$CE[bp],ax
!BCC_EOS
! 6     int i=0;
dec	sp
dec	sp
! Debug: eq int = const 0 to int i = [S+$D2-$D2] (used reg = )
xor	ax,ax
mov	-$D0[bp],ax
!BCC_EOS
! 7     int j=0;
dec	sp
dec	sp
! Debug: eq int = const 0 to int j = [S+$D4-$D4] (used reg = )
xor	ax,ax
mov	-$D2[bp],ax
!BCC_EOS
! 8 
! 9     for(i=0;i<50;i++){
! Debug: eq int = const 0 to int i = [S+$D4-$D2] (used reg = )
xor	ax,ax
mov	-$D0[bp],ax
!BCC_EOS
!BCC_EOS
jmp .3
.4:
! 10       in[i]=i*5+28%25;
! Debug: mul int = const 5 to int i = [S+$D4-$D2] (used reg = )
mov	ax,-$D0[bp]
mov	dx,ax
shl	ax,*1
shl	ax,*1
add	ax,dx
! Debug: add int = const 3 to int = ax+0 (used reg = )
add	ax,*3
push	ax
! Debug: ptradd int i = [S+$D6-$D2] to [$32] int in = S+$D6-$6A (used reg = )
mov	ax,-$D0[bp]
shl	ax,*1
mov	bx,bp
add	bx,ax
! Debug: eq int (temp) = [S+$D6-$D6] to int = [bx-$68] (used reg = )
mov	ax,-$D4[bp]
mov	-$68[bx],ax
inc	sp
inc	sp
!BCC_EOS
! 11     }
! 12 
! 13     for(i=0;i<50;i++){
.2:
! Debug: postinc int i = [S+$D4-$D2] (used reg = )
mov	ax,-$D0[bp]
inc	ax
mov	-$D0[bp],ax
.3:
! Debug: lt int = const $32 to int i = [S+$D4-$D2] (used reg = )
mov	ax,-$D0[bp]
cmp	ax,*$32
jl 	.4
.5:
.1:
! Debug: eq int = const 0 to int i = [S+$D4-$D2] (used reg = )
xor	ax,ax
mov	-$D0[bp],ax
!BCC_EOS
!BCC_EOS
jmp .8
.9:
! 14         avg=0;
! Debug: eq int = const 0 to int avg = [S+$D4-$D0] (used reg = )
xor	ax,ax
mov	-$CE[bp],ax
!BCC_EOS
! 15         for(j=i-4;j<=i+5;j++){
! Debug: sub int = const 4 to int i = [S+$D4-$D2] (used reg = )
mov	ax,-$D0[bp]
! Debug: eq int = ax-4 to int j = [S+$D4-$D4] (used reg = )
add	ax,*-4
mov	-$D2[bp],ax
!BCC_EOS
!BCC_EOS
jmp .C
.D:
! 16             if((j>=0)&&(j<50)){
! Debug: ge int = const 0 to int j = [S+$D4-$D4] (used reg = )
mov	ax,-$D2[bp]
test	ax,ax
jl  	.E
.10:
! Debug: lt int = const $32 to int j = [S+$D4-$D4] (used reg = )
mov	ax,-$D2[bp]
cmp	ax,*$32
jge 	.E
.F:
! 17                 avg=avg+in[j];
! Debug: ptradd int j = [S+$D4-$D4] to [$32] int in = S+$D4-$6A (used reg = )
mov	ax,-$D2[bp]
shl	ax,*1
mov	bx,bp
add	bx,ax
! Debug: add int = [bx-$68] to int avg = [S+$D4-$D0] (used reg = )
mov	ax,-$CE[bp]
add	ax,-$68[bx]
! Debug: eq int = ax+0 to int avg = [S+$D4-$D0] (used reg = )
mov	-$CE[bp],ax
!BCC_EOS
! 18             }
! 19             avg=avg/10;
.E:
! Debug: div int = const $A to int avg = [S+$D4-$D0] (used reg = )
mov	ax,-$CE[bp]
mov	bx,*$A
cwd
idiv	bx
! Debug: eq int = ax+0 to int avg = [S+$D4-$D0] (used reg = )
mov	-$CE[bp],ax
!BCC_EOS
! 20             res[i]=avg;
! Debug: ptradd int i = [S+$D4-$D2] to [$32] int res = S+$D4-$CE (used reg = )
mov	ax,-$D0[bp]
shl	ax,*1
mov	bx,bp
add	bx,ax
! Debug: eq int avg = [S+$D4-$D0] to int = [bx-$CC] (used reg = )
mov	ax,-$CE[bp]
mov	-$CC[bx],ax
!BCC_EOS
! 21         }
! 22     }
.B:
! Debug: postinc int j = [S+$D4-$D4] (used reg = )
mov	ax,-$D2[bp]
inc	ax
mov	-$D2[bp],ax
.C:
! Debug: add int = const 5 to int i = [S+$D4-$D2] (used reg = )
mov	ax,-$D0[bp]
! Debug: le int = ax+5 to int j = [S+$D4-$D4] (used reg = )
add	ax,*5
cmp	ax,-$D2[bp]
jge	.D
.11:
.A:
! 23     return 0;
.7:
! Debug: postinc int i = [S+$D4-$D2] (used reg = )
mov	ax,-$D0[bp]
inc	ax
mov	-$D0[bp],ax
.8:
! Debug: lt int = const $32 to int i = [S+$D4-$D2] (used reg = )
mov	ax,-$D0[bp]
cmp	ax,*$32
blt 	.9
.12:
.6:
xor	ax,ax
lea	sp,-4[bp]
pop	si
pop	di
pop	bp
ret
!BCC_EOS
! 24 }
! 25 
! Register BX used in function main
.data
.bss

! 0 errors detected
